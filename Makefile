all: play.it_conf-2017-11-25.pdf

play.it_conf-2017-11-25.pdf: src/play.it_conf-2017-11-25.tex src/img/banniere.png src/img/avatar-vv221.png src/img/logo-windows.png src/img/logo-linux.png src/img/logo-debian.png src/img/demo-script.png src/img/logo-archlinux.png src/img/web-www.png src/img/web-wiki.png src/img/logo-framasoft.png src/img/demo-game.jpg src/img/nethack.png src/img/logo-loki.jpg src/img/logo-steam.png
	xelatex src/play.it_conf-2017-11-25.tex
	xelatex src/play.it_conf-2017-11-25.tex

src/img/logo-windows.png: src/img/logo-windows.svg
	rsvg-convert --width 512 --output src/img/logo-windows.png --keep-aspect-ratio src/img/logo-windows.svg

src/img/logo-linux.png: src/img/logo-linux.svg
	rsvg-convert --width 512 --output src/img/logo-linux.png --keep-aspect-ratio src/img/logo-linux.svg

src/img/logo-debian.png: src/img/logo-debian.svg
	rsvg-convert --width 512 --output src/img/logo-debian.png --keep-aspect-ratio src/img/logo-debian.svg

src/img/logo-archlinux.png: src/img/logo-archlinux.svg
	rsvg-convert --height 256 --output src/img/logo-archlinux.png --keep-aspect-ratio src/img/logo-archlinux.svg

src/img/logo-framasoft.png: src/img/logo-framasoft.svg
	rsvg-convert --width 512 --output src/img/logo-framasoft.png --keep-aspect-ratio src/img/logo-framasoft.svg

src/img/logo-steam.png: src/img/logo-steam.svg
	rsvg-convert --width 512 --output src/img/logo-steam.png --keep-aspect-ratio src/img/logo-steam.svg

clean:
	rm -f *.aux *.log *.nav *.out *.snm *.toc src/img/logo-windows.png src/img/logo-linux.png src/img/logo-debian.png src/img/logo-archlinux.png src/img/logo-framasoft.png src/img/logo-steam.png
